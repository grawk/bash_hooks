# bash hooks

A bash "framework" which makes hooks easy. Useful for writing tests or other scripts 
that need to go in a certain order without having to create `010_runthis.sh`, `020_run_that.sh`, etc


all you need to do is source the file, set the hook order, enable the hooks you want to run dynamically
and run hooks

## simple example

```
#!/bin/bash

source bash_hooks.sh

hook_order_enable init sometest anothertest bleh

function _run_bleh() {
    echo "running bleh"
}

function _run_sometest() {
  echo "running some test"
}

function _run_anothertest() {
  echo "running another test"
}

function _run_init() {
  echo "initializing..."
}

run_hooks
```

The output of running this is:

```
initializing...
running some test
running another test
running bleh
```

More examples are in the `./examples` directory

# TODO better doc
