#!/bin/bash -ex

# set this ins script if you want to run hooks as individual scripts
# from a specific folder
#HOOK_FOLDER='/path/to/somefolder'

HOOK_PREFIX=${HOOK_PREFIX:-"_run_"}
declare -A FUNC_ARGS
declare -A ENABLED_HOOKS
HOOK_ORDER=()


# Only these hooks will run in this order
function hook_order() {
  HOOK_ORDER=$@
}

function enable_hooks() {
  for hook in $@; do
    ENABLED_HOOKS["${hook}"]=1
  done;
}

# set hook order and enable them to run at the same time
# useful if you don't really plan on having hooks added dynamically
function hook_order_enable() {
  hook_order $@
  enable_hooks $@
}

# Enable hook and set its arguments
function set_hook() {
  if [ -z ${1+_} ]; then
    echo "set_hook expects at least 1 argument" >&2
    exit
  fi

  hook=$1
  enable_hooks $hook
  if [ ${2+_} ]; then
    FUNC_ARGS["${hook}"]="${@:2}"
  fi
}

function run_infile_hook() {
  func=$1
  $HOOK_PREFIX$func ${FUNC_ARGS[$func]}
}

function run_file_hook() {
  func=$1
  hook_file=${HOOK_FOLDER}/"${HOOK_PREFIX}${func}".sh
  if [ ! -f $hook_file ]; then
    echo "Could not find executable hook at ${hook_file}" >&2
    exit 1
  fi
  $hook_file ${FUNC_ARGS[$func]}
}

function run_hooks() {
  if [ ! -z ${HOOK_FOLDER+_} ]; then
    if [ ! -d ${HOOK_FOLDER} ]; then
      echo "Could not find hook directory at ${HOOK_FOLDER} !" >&2
      exit 1
    fi;
  fi

  declare -A FUNC_STACK
  for func in ${HOOK_ORDER[*]}; do
    # Precaution so we don't run a hook more than once 
    # TODO: implement retry?
    if [ ${FUNC_STACK[$func]+_} ]; then
      continue;
    fi

    # Only run hooks we want to
    if [ -z ${ENABLED_HOOKS[$func]+_} ]; then
      continue;
    fi

    if [ ! -z ${HOOK_FOLDER+_} ] && [ -d ${HOOK_FOLDER} ]; then
      run_file_hook $func 
    else
      run_infile_hook $func
    fi
  
    FUNC_STACK["${func}"]=1
  done;
}


