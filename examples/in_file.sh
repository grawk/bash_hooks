#!/bin/bash

# example of how bash_hooks can be used to run hooks in-file by utilizing a function prefix

function usage() {
  cat <<EOF
Example bash hook script

options:
  -l: run 'ls -la' on given directory
  -p: run 'ps aux'
EOF
  exit 1
}

# hook prefix can be overriden by adding this line
# HOOK_PREFIX='_otherprefix_'
source $(dirname $0)/../bash_hooks.sh

# only these hooks will run in this order
hook_order init sometest footest psfunc lsfunc wrapup
# they must first be enabled though
enable_hooks init wrapup

while getopts "l:ph" opt; do
  case "${opt}" in
    l)
      set_hook "lsfunc" ${OPTARG}
      ;;
    p)
      set_hook "psfunc"
      ;;
    h)
      usage
      ;;
  esac
done


function _run_lsfunc() {
  echo running ls
  ls -la $1
}

function _run_psfunc() {
  ps aux
}

function _run_wrapup() {
  echo "cleaning stuff up"
}

function _run_init() {
  echo "performing some initialization..."
}



run_hooks
