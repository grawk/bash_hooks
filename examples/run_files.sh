#!/bin/bash

# example of how bash_hooks can be used with individual files

# Just an example for injecting optional hooks
optional_hooks=(footest say)

# hook prefix can be overriden by adding this after sourcing bash_hooks
HOOK_PREFIX="hook_"
HOOK_FOLDER=" $(dirname $0)/hook_scripts"
source $(dirname $0)/../bash_hooks.sh

# only these hooks will run in this order
hook_order init sometest ${optional_hooks[@]} wrapup
# they must first be enabled though
enable_hooks init sometest wrapup


if [ ! -z $1 ]; then
  if [ ! "$(echo ' '${optional_hooks[@]}' ' | egrep ' '$1' ')" ]; then 
    echo "invalid optional hook. valid values are:" ${optional_hooks[@]}
    exit 1
  fi
  # allow a hook to be enabled via cmdline
  set_hook $1 ${@:2}
fi


run_hooks
